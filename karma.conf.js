// Karma configuration
// Generated on Thu Jun 25 2015 19:37:18 GMT+0530 (IST)

module.exports = function(config) {
  config.set({
    basePath: '',
    frameworks: ['jasmine'],
    
    files: [

        'bower_components/angular/angular.js',
        'bower_components/angular-mocks/angular-mocks.js',
        'public/controllers/*.js',
        'test/*.spec.js',
        'public/directives/**/*'

    ],

    reporters: ['progress'],
    // web server port
    port: 9876,

    colors: true,

    
    preprocessors: {
        'public/directives/**/*.html': 'ng-html2js'
    },

     ngHtml2JsPreprocessor: {

        moduleName: "app"
      },

    logLevel: config.LOG_INFO,


    // enable / disable watching file and executing tests whenever any file changes
    autoWatch: true,


    // start these browsers
    // available browser launchers: https://npmjs.org/browse/keyword/karma-launcher
    browsers: ['Chrome'],


    // Continuous Integration mode
    // if true, Karma captures browsers, runs the tests and exits
    singleRun: false
  })
}
