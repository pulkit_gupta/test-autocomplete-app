
var development = {
  
  appURL : 'localhost',
  
  timeZone: 5.5,
  
  
  port : 9000,

  /* DB settings */
  ip:"localhost ",   //localhost  
 

  env : global.process.env.NODE_ENV || 'development'
};


var production = {

  appURL : 'www.opdemivia.de',
  
  timeZone: 5.5,
  port : 9000,  
  
  env : global.process.env.NODE_ENV || 'development'
};


exports.Config = global.process.env.NODE_ENV === 'production' ? production : development;