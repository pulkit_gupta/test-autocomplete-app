describe('directiveTest', function(){
	
	beforeEach(function(){module('app')});

	var scope ,rootScope, element, dScope;

	beforeEach(module('app'));
	    beforeEach(inject(function ($rootScope, $compile) {

	    	rootScope = $rootScope;
	        scope = $rootScope.$new(),
	            

	        element = $compile(angular.element('<search-box type="mock"></search-box>'))(scope);

	        scope.$digest();
	        
	        //dScope = element.isolateScope();
	    }));

	    

	it("Initial values of index to be -1 for search", inject(function() {
        
        
        //expect(element.isolateScope()).toBeDefined();
        	
        expect(scope.searchIndex).toBe(-1);
        
        //expect(element.html()).toContain('a');
        // expect($scope.open).toBeTruthy();
        // $scope.toggle();
         
    }));

});