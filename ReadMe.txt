Brief description

Bootsrap (For responsive design) used for HTML page extended with Custom.css 

Angular directive is written for the searchbox with autoomplete functionality with flexible API to be used.

Not required but very simple and small NodeJS server is also written.

Bower package manager is used for frontend and NPM is used for other tools like karma and jasime.

Public folder is the front end folder in this case which has folders:

directives, controllers, index.html and css folder


Usage:

Syntax for this search box is

<search-box type="google"> </search-box>

where type can be changed depending upo the data set of the suggestion list for autocompletetion.

In this way, N number of search boxes can be used on a web page.



General work information

Node_modules are also uploaded to git. Done for ease of test. (Generaly not recommended).

Starting GIT commits are done targetting different controllers but that is not efficient code. Though In some scenario it is better to write separate controller.

In last commit, i changed the code structure with directive which is very handy to use and place as many search box available.


Unit Test environment using karma and jasmine are used 
Test cases are not written.


For further information:

Contact: Pulkit Gupta

Email: pulkit.itp@gmail.com






