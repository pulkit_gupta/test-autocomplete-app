
/*
Name: Services
Author: Pulkit Gupta
Email: pulkit.itp@gmail.com
Function: 
*/

'use strict';
(function(angular){

	angular.module('app').service('server', function($http){	
		console.log("calling client global var service");
		return	{ 

			/*Dummy data used for the mock type search*/
			dummyData: [
			"C++",
			"C",
			"Java",
			"JSP",
			"J2EE",
			"Python",
			"HTML",
			"CSS",
			"LESS",
			"Jquery",
			"AngularJS",
			"BackboneJS",
			"Javascript"
			],

			/*Gooogle API to get suggestion via JSONP*/
			getLiveData: function(keyword, callback){

				//var URI = "http://suggestqueries.google.com/complete/search?client=firefox&q=superstar";
				$.ajax({
					url: 'http://suggestqueries.google.com/complete/search?client=chrome&q='+keyword,
					type: 'GET',
					dataType: 'jsonp',
					success: function (data) {
				   // console.log(data[1]);
				   callback(data[1]);
				},
					error: function(jqXHR, textStatus, errorThrown){
						console.log(jqXHR);
						console.log(textStatus);
						console.log(errorThrown);
					}
				});


				//Equivalent angular function (Small problem)

				/*$http.jsonp(
				  'http://suggestqueries.google.com/complete/search?q=sheikh+hasina&hl=EN&cp=1&gs_id=6&xhr=t&callback=JSON_CALLBACK'
				     ).success(
				       function(data) {
				         $scope.dataset = data[1] || [];
				         console.log("data came");
				         console.log(data+" recieved");
				         callback(data[1])
		       		}
		    	);*/
			}
		};
	});
})(angular);
