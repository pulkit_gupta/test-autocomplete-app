
/*
Name: Main controller(Future)
Author: Pulkit Gupta
Email: pulkit.itp@gmail.com
Function: Initializing app module for the first time.
*/

'use strict';

(function(angular){
	var app = angular.module('app',[]);
})(angular);


