
/*
Name: Search Box directive
Author: Pulkit Gupta
Email: pulkit.itp@gmail.com
Function: Logic for the suggestion list for the search field and connectivity to the respective API for data.
*/

'use strict';

(function (angular) {

    //getting the app module and injecting the server service into it for accessing the Google API
    angular.module('app').directive('searchBox', ['server', function (server) {
        //prepate empty directive object to be returned
        var directive = {};

        //restricting the use by Html Element
        directive.restrict = 'E';

        //Html code for this directive
        directive.templateUrl = 'directives/searchBox/searchBox.html';
        directive.scope = {
            type: '@type'  //type to get the API type
        };
        
        /*Controller linked with this directive*/
        directive.link = function ($scope, element, attrs ) {
            
            /*Empty object for all the items to be showm in suggestion list.*/
            $scope.searchItems = {};
              
            /* Function to call google API  */
            var googleService = function(){
                server.getLiveData($scope.searchText, function(resp){

                    /*Empty suggestion array to be filled by google suggestions*/
                    $scope.suggestions = [];   
                    
                    /* UNCOMMENT if want all results to be shown */
                    //$scope.suggestions_real = resp;

                    //change the value from 5 to n for the amount of result in suggestion list
                    for(var j = 0; j<5; j++){
                        //check for the result avaiability
                        if(resp[j]){
                            $scope.suggestions.push(resp[j]);
                        }
                    }
                    
                    //Not recommended but is not updating the view at once if not used :(
                    $scope.$apply();
                });
            }  

            /* Function to use the mock data in JSON for suggestion list during search*/
            var mockFunction = function(){
                $scope.suggestions = [];
                
                /* variable used for the suggestion list maximum number*/
                var myMaxSuggestionListLength = 0;

                for(var i=0; i<$scope.searchItems.length; i++){
                    var searchSmallLet = angular.lowercase($scope.searchItems[i]);

                    //making the case insensitivity
                    var searchTextSmallLetters = angular.lowercase($scope.searchText);

                    if( searchSmallLet.indexOf(searchTextSmallLetters) ===0){

                        $scope.suggestions.push(searchSmallLet);
                        myMaxSuggestionListLength += 1;
                        //break once reach the maximum allowed suggestions
                        if(myMaxSuggestionListLength == 5){
                            break;
                        }
                    }
                }
            }    


             /* Service Selection according to the type of search box*/
            switch ($scope.type) {
                case "google":
                    //console.log('I m google search ');
                    $scope.search = googleService;
                    break;

                case "mock":
                    //console.log('I am mock search');
                    $scope.searchItems = server.dummyData;
                    $scope.searchItems.sort();
                    $scope.search = mockFunction;
                    break;

                default :
                    throw new Error({error:"unknown search type defined"});
            }


            /* Initializing the variable suggestion array and index holder for the usage*/
            $scope.suggestions = [];
            $scope.selectedIndex = -1;
            
            
            //Keep track of search text value during the selection from the suggestions List  
            $scope.$watch('selectedIndex',function(val){
                if(val !== -1) {
                    $scope.searchText = $scope.suggestions[$scope.selectedIndex];
                }
            });
            
            
        
            /*funtion to call ng-keydown*/

            $scope.checkKeyDown = function(event){
                if(event.keyCode === 40){//40 for down key and  increment selectedIndex
                    event.preventDefault();
                    if($scope.selectedIndex+1 !== $scope.suggestions.length){
                        $scope.selectedIndex++;
                    }
                }else if(event.keyCode === 38){ //30 for up key decrements selectedIndex
                    event.preventDefault();
                    if($scope.selectedIndex-1 !== -1){
                        $scope.selectedIndex--;
                    }
                }else if(event.keyCode === 13){ //enter key with 13 used to empty suggestions array
                    event.preventDefault();
                    $scope.suggestions = [];
                }
            }

            /*Function to call on ng-keyup*/
            $scope.checkKeyUp = function(event){ 
                if(event.keyCode !== 8 || event.keyCode !== 46){//delete or backspace
                    if($scope.searchText == ""){
                        $scope.suggestions = [];
                    }
                }

            }

            /*Function to assign the value to the main field when clicked enter or clicked.
                This also empty the suggestion array.
            */
            $scope.assignAndHide = function(index){
                 $scope.searchText = $scope.suggestions[index];
                 $scope.suggestions=[];
            }

        };
        //Finally return the prepared directive object.
        return directive;
    }])
})(angular);



