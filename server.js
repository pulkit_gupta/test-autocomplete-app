 /*
  Function: Serves Quandoo app 
  Interacts with DB: Yes
  Entry point: main http server call
  Configuration file: config.js

  Author: Pulkit Gupta
  Query email: pulkit.itp@gmail.com
 */


var api = require('./routes/api');

var express = require('express');
var http = require('http');

/* GLOBAL VARIABLE */
var config = require('./conf').Config

var app = express();

/*Global variable */

app.configure(function(){
  this.use(express.json());
  //this.use(express.bodyParser());
  this.use(express.json());
  this.use(express.urlencoded());
  this.use(express.static(__dirname + '/public'));
});



/* Http request*/
app.post('/login', api.search); 

var server = http.createServer(app);
//starts server
server.listen(config.port, function () {
  console.log('listening on '+config.ip+ ':'+config.port);
  console.log("running with domain name:"+ config.appURL);
  console.log("DB name:"+ config.db_name);
  //reconnect("dst1");

});


